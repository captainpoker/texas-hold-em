import javafx.scene.control.Button;
import javafx.scene.layout.BackgroundFill;

public class CardButton extends Button{
	private String buttonText;
	public CardButton(String s){
		super(s);
		setButtonText(s);
	}
	public String getButtonText() {
		return buttonText;
	}
	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}
	
	public Card cardButtonToCard(CardButton x){
		String[] sub = x.getText().split(" ");
		return new Card(sub[2], sub[0]);
	}

	
	
}
