import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
//something wrong with if in call and player1 bet 
//add turns 
import javafx.util.Pair;
public class Hand extends Application{
	//initialize stage and scene
	private Stage primary;
	private GridPane pane;
	private GridPane panel1;
	private Scene player1end;
	private Scene player2end;
	private GridPane player1endpane;
	//initializes bets
	private int player1bet;// = 2000;
	private int player2bet;// = 2000;
	private int player3bet = 2000;
	private int player4bet = 2000;
	private int currentbettingvalue;
	//initializes arraylists of cards
	private ArrayList<CardButton> player1TwoCards;// = new ArrayList();
	private ArrayList<CardButton> player2TwoCards;// = new ArrayList();
	private ArrayList<CardButton> player30Cards;// = new ArrayList();
	private ArrayList<CardButton> player40Cards = new ArrayList();
	private ArrayList<CardButton> player1Cards;// = new ArrayList();
	private ArrayList<CardButton> player2Cards;// = new ArrayList();
	private ArrayList<CardButton> player3Cards = new ArrayList();
	private ArrayList<CardButton> player4Cards = new ArrayList();
	private ArrayList<CardButton> flop;// = new ArrayList();
	private ArrayList<CardButton> turn;// = new ArrayList();
	private ArrayList<CardButton> river;// = new ArrayList();
	private ArrayList<CardButton> flop1;// = new ArrayList();
	private ArrayList<CardButton> turn1;// = new ArrayList();
	private ArrayList<CardButton> river1;// = new ArrayList();
	private ArrayList<Card> tempdeck;
	private ArrayList<Card> tempdeck2;
	private Card [] temp1;
	private Card [] temp2;
	//turn counter and decks for flop turn and river
	private int turns;// = 0;
	private int cardpick = 0;
	private Deck player1deck;// = new Deck(2);
	private Deck player2deck;// = new Deck(2);
	private Deck d;// = new Deck(3);	
	private Deck f;// = new Deck(1);
	private Deck g;// = new Deck(1);
	private int player1currentbet;
	private int player2currentbet;
	GridPane player1pane;
	Scene player1;
	GridPane player2pane;
	Scene player2;
	public static void main(String [] args){
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//initialize stage and panes
		primary = primaryStage;
		panel1 = new GridPane();
		pane = new GridPane();
		//set scenes
		Scene scene = new Scene(pane, 300, 500);
		Scene s1 = new Scene(panel1, 400, 400);
		//buttons for players
		Button first = new Button("Two Players");
		Button second = new Button("Three Players");
		Button third = new Button("Four Players");
		//decides whos playing	
		first.setOnAction(e -> {
			primary.setScene(scene);
			twoPlayerGame();
		});
			second.setOnAction(e ->{
				//primary.setScene(scene);
				//primary.1PlayerGame();
			});
			
			third.setOnAction(e ->{
				//primary.setScene(scene);
				//primary.2PlayerGame();
			});
			
		//change to grid and add buttons
		primary.setTitle("Poker Game");
		primary.setScene(s1);
		panel1.add(first, 0, 2);
		Label l6 = new Label("Three and Four Player Game are Currently Unavailable");
		panel1.add(l6, 0, 3);
		panel1.add(second,  0,  4);
		panel1.add(third,  0,  5);
		first.setAlignment(Pos.CENTER);
		second.setAlignment(Pos.CENTER);
		third.setAlignment(Pos.CENTER);
		Label l = new Label("How Many People Are Playing?");
		l.setAlignment(Pos.CENTER);
		panel1.getChildren().add(l);
		primary.show();
		}
		//scene for each player, integer to keep track of betting
	//stated at top of scene
	
	public void twoPlayerGame(){
		//adds player 1 and two cards to scenes
		player1pane = new GridPane();
		player1 = new Scene(player1pane, 500, 300);
		player2pane = new GridPane();
		player2 = new Scene(player2pane, 500, 300);
		primary.setScene(player1);
		player1bet = 2000;
		player2bet = 2000;
		currentbettingvalue = 100;
		player1TwoCards = new ArrayList();
		player2TwoCards = new ArrayList();
		player1Cards = new ArrayList();
		player2Cards = new ArrayList();
		flop = new ArrayList();
		turn = new ArrayList();
		river = new ArrayList();
		flop1 = new ArrayList();
		turn1 = new ArrayList(); 
		river1 = new ArrayList();
		turns = 0;
		player1deck = new Deck(2);
		player2deck = new Deck(2);
		d = new Deck(3);
		f = new Deck(1);
		g = new Deck(1);
		player1currentbet = 0;
		player2currentbet = 0;
		//buttons for the scene
			//int currentbettingvalue = 0;
		Label l = new Label("Player 1's turn. Would you like to bet or call?                 ");
		Button fold = new Button("Fold");
		Button bet50 = new Button("Bet $50");
		Button call = new Button("Call");
		Label player1money = new Label("Current Amount of Money: " + player1bet);
		Label player2money = new Label("Current Amount of Money: " + player2bet);
		Label player2betlabel = new Label("Current Betting Amount: " + currentbettingvalue);
		Label player1betlabel = new Label("Current Betting Amount: " + currentbettingvalue);
		
		while(player2TwoCards.size() < 2){
			player1TwoCards.add(player1deck.getCard(0).cardToCardButton(player1deck.getCard(0)));
			player1TwoCards.add(player1deck.getCard(1).cardToCardButton(player1deck.getCard(1)));
			if(player1deck.getCard(0).compareTo(player2deck.getCard(0)) == 0 || player1deck.getCard(1).compareTo(player2deck.getCard(0)) == 0){
				player2deck = new Deck();
			}
			else{
				if(player1deck.getCard(0).compareTo(player2deck.getCard(1)) == 0 || player1deck.getCard(1).compareTo(player2deck.getCard(1)) == 0){
					player2deck = new Deck();
				}
				else{
					player2TwoCards.add(player2deck.getCard(0).cardToCardButton(player2deck.getCard(0)));
					player2TwoCards.add(player2deck.getCard(1).cardToCardButton(player2deck.getCard(1)));
				}
			}
		}
		//adds buttons to scenes
		player1pane.clearConstraints(player1TwoCards.get(0));
		player1pane.clearConstraints(player1TwoCards.get(1));
		player1pane.add(fold, 0, 5);
		player1pane.add(bet50,  0,  6);
		player1pane.add(call,  0,  7);
		player1pane.add(l, 0, 1);
		player1pane.add(player1money,  1,  1);
		player1pane.add(player1betlabel,  1,  5);
		player1pane.add(player1TwoCards.get(0),  1,  8);
		player1pane.add(player1TwoCards.get(1),  1,  9);
			
		
		primary.show();
		//adds buttons to scenes
		Label l1 = new Label("Player 2's turn. Would you like to bet or call?                      ");
		Button fold1 = new Button("Fold");
		Button bet501 = new Button("Bet $50");
		Button call1 = new Button("Call");
		player2pane.clearConstraints(player2TwoCards.get(0));
		player2pane.clearConstraints(player2TwoCards.get(1));
		player2pane.add(fold1, 0, 5);
		player2pane.add(bet501,  0,  6);
		player2pane.add(call1,  0,  7);
		player2pane.add(l1, 0, 1);
		player2pane.add(player2money,  1,  1);
		player2pane.add(player2betlabel,  1,  5);
		player2pane.add(player2TwoCards.get(0),  1,  8);
		player2pane.add(player2TwoCards.get(1),  1,  9);
		fold.setOnAction(e ->{
			//adds buttons and labels and ends the game if needed
			GridPane thankyou = new GridPane();
			Scene thanks = new Scene(thankyou, 400, 400);
			Label t = new Label("Thanks for Playing! Play Again?");
			Button yes = new Button("Play Again");
			Button no = new Button("Close the Game");
			thankyou.add(yes, 0, 4);
			thankyou.add(no,  0,  5);
			thankyou.getChildren().add(t);
			primary.setScene(thanks);
			yes.setOnAction(p ->{
				try {
					start(primary);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					
				}
			});
			
			no.setOnAction(p ->{
				System.exit(0);
			});
		});
		//ends game if needed and puts stuff into another pane
		fold1.setOnAction(e ->{
			GridPane thankyou = new GridPane();
			Scene thanks = new Scene(thankyou, 400, 400);
			Label t = new Label("Thanks for Playing! Play Again?");
			Button yes = new Button("Play Again");
			Button no = new Button("Close the Game");
			thankyou.add(yes, 0, 4);
			thankyou.add(no,  0,  5);
			thankyou.getChildren().add(t);
			primary.setScene(thanks);
			yes.setOnAction(p ->{
				try {
					start(primary);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					
				}
			});
			
			no.setOnAction(p ->{
				System.exit(0);
			});
		});
		bet50.setOnAction(e ->{
			//confirmation screen
			GridPane okay = new GridPane();
			Scene okays = new Scene(okay, 400,400);
			Button nextTurn = new Button("Next Players Turn");
			okay.add(nextTurn,  0, 5);
			Label changed = new Label("New Betting Amount: " + (currentbettingvalue + 50));
			player1money.setText("Current Amount of Money: " + (player1bet-50));
			player1betlabel.setText("Current Pot: " + (currentbettingvalue + 50));
			okay.add(changed,  0, 0);
			primary.setScene(okays);
			player1bet -= 50;
			currentbettingvalue += 50;
			player1currentbet += 50;
			if(turns == 6){//if everyone has bet and all cards on table
	cardPicker(1);
				}
				//primary.setScene(player2);
		
			else{//add cards to player 2 and switch screens
				if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					player2pane.add(turn.get(0), 0, 11);
					player2pane.add(river.get(0), 0, 12);
				}
				else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					player2pane.add(turn.get(0), 0, 11);
					
				}
				else if(flop.size() == 3 && turn.size() != 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					
				}
			}
			++turns;
			nextTurn.setOnAction(p ->{
				primary.setScene(player2);
			});
			
		});
		bet501.setOnAction(e ->{
			//confirmation screen
			GridPane okay = new GridPane();
			Scene okays = new Scene(okay, 400,400);
			Button nextTurn = new Button("Next Players Turn");
			okay.add(nextTurn,  0, 5);
			Label player1moneychanged = new Label("New Betting Value: " + (currentbettingvalue + 50));
			player2money.setText("Current Amount of Money: " + (player2bet-50));
			player2betlabel.setText("Current Pot: " + (currentbettingvalue + 50));
			okay.add(player1moneychanged,  0, 0);
			currentbettingvalue += 50;
			if(player1currentbet - player2currentbet != 0){
			player2bet = player2bet-100;
			player2currentbet += 50;
			}
			//currentbettingvalue += 50;
			primary.setScene(okays);
			
			if(turns == 6){//if all cards down and everyone has bet
				cardPicker(2);
				
			}
			else{//put cards on player 2 and switch scenes
			while(flop.size() ==3 && turn.size() == 1 && river.size() != 1){
				if(player1deck.getCard(0).compareTo(g.getCard(0)) == 0 || 
						player1deck.getCard(1).compareTo(g.getCard(0)) == 0 ||
						player2deck.getCard(0).compareTo(g.getCard(0)) == 0 || 
						player2deck.getCard(1).compareTo(g.getCard(0)) == 0  )
				{
					g = new Deck(1);
				}
				else{
					river.add(g.getCard(0).cardToCardButton(g.getCard(0)));
				    river1.add(g.getCard(0).cardToCardButton(g.getCard(0)));
				}
			}
			while(flop.size() == 3 && turn.size() != 1){
				if(player1deck.getCard(0).compareTo(f.getCard(0)) == 0 || 
						player1deck.getCard(1).compareTo(f.getCard(0)) == 0 ||
						player2deck.getCard(0).compareTo(f.getCard(0)) == 0 || 
						player2deck.getCard(1).compareTo(f.getCard(0)) == 0  )
				{
					f = new Deck(1);
				}
				else{
					turn.add(f.getCard(0).cardToCardButton(f.getCard(0)));
					turn1.add(f.getCard(0).cardToCardButton(f.getCard(0)));
			}
			}
			while(flop.size() < 3){
				if(player1deck.getCard(0).compareTo(d.getCard(0)) == 0 || 
						player1deck.getCard(1).compareTo(d.getCard(0)) == 0 ||
						player2deck.getCard(0).compareTo(d.getCard(0)) == 0 || 
						player2deck.getCard(1).compareTo(d.getCard(0)) == 0  )
				{
					d = new Deck(3);
				}
				else{
					if((player1deck.getCard(0).compareTo(d.getCard(1)) == 0 || 
							player2deck.getCard(1).compareTo(d.getCard(1)) == 0 ||
							player2deck.getCard(0).compareTo(d.getCard(1)) == 0 || 
							player1deck.getCard(1).compareTo(d.getCard(1)) == 0  ))
							{
						d = new Deck(3);
					}
					else{
						if((player1deck.getCard(0).compareTo(d.getCard(2)) == 0 || 
								player2deck.getCard(1).compareTo(d.getCard(2)) == 0 ||
								player2deck.getCard(0).compareTo(d.getCard(2)) == 0 || 
								player1deck.getCard(1).compareTo(d.getCard(2)) == 0  )){
							d = new Deck(3);
						}
						else{
							flop.add(d.getCard(0).cardToCardButton(d.getCard(0)));
							flop.add(d.getCard(1).cardToCardButton(d.getCard(1)));
							flop.add(d.getCard(2).cardToCardButton(d.getCard(2)));
							flop1.add(d.getCard(0).cardToCardButton(d.getCard(0)));
							flop1.add(d.getCard(1).cardToCardButton(d.getCard(1)));
							flop1.add(d.getCard(2).cardToCardButton(d.getCard(2)));
						}
					}
				}
				
			}
			if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
				for(int i = 0; i<flop.size(); i++){
					player1pane.add(flop.get(i), 0, 8+i);
				}
				player1pane.add(turn.get(0), 0, 11);
				player1pane.add(river.get(0), 0, 12);
			}
			else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
				for(int i = 0; i<flop.size(); i++){
					player1pane.add(flop.get(i), 0, 8+i);
				}
				player1pane.add(turn.get(0), 0, 11);
				
			}
			else if(flop.size() == 3 && turn.size() != 1){
				for(int i = 0; i<flop.size(); i++){
					player1pane.add(flop.get(i), 0, 8+i);
				}
				
			}
			
			}
			++turns;//adds to played turns and sets scene to next player
			nextTurn.setOnAction(p ->{
				primary.setScene(player1);
			});
			
		}
	);
	
	
		
		call.setOnAction(e ->{
			//confirmation
			GridPane okay = new GridPane();
			Scene okays = new Scene(okay, 400,400);
			Button nextTurn = new Button("Next Players Turn");
			okay.add(nextTurn,  0, 5);
			Label player1moneychanged = new Label("New Betting Value: " + (player1bet-currentbettingvalue));
			okay.add(player1moneychanged,  0, 0);
			primary.setScene(okays);
if(turns == 6){//if everyone has bet
cardPicker(1);	
}		else{//add cards to player 1 and change scenes
				
				if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					player2pane.add(turn.get(0), 0, 11);
					player2pane.add(river.get(0), 0, 12);
				}
				else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					player2pane.add(turn.get(0), 0, 11);
					
				}
				else if(flop.size() == 3 && turn.size() != 1){
					for(int i = 0; i<flop.size(); i++){
						player2pane.add(flop.get(i), 0, 8+i);
					}
					
				}
			}
++turns;
			nextTurn.setOnAction(p ->{//change scene to next player
				primary.setScene(player2);
		});
		});
call1.setOnAction(e ->{
	//confirmation
	GridPane okay = new GridPane();
	Scene okays = new Scene(okay, 400,400);
	Button nextTurn = new Button("Next Players Turn");
	okay.add(nextTurn,  0, 5);
	Label player1moneychanged = new Label("New Betting Value: " + (player2bet-currentbettingvalue));
	okay.add(player1moneychanged,  0, 0);
	//currentbettingvalue += 50;
	if(player1currentbet - player2currentbet != 0){
	player2bet = player2bet-50;
	player2currentbet += 50;
	}
	//currentbettingvalue += 50;
	primary.setScene(okays);
	if(turns == 6){
	
	cardPicker(2);
	}
	else{
		while(flop.size() ==3 && turn.size() == 1 && river.size() != 1){
			if(player1deck.getCard(0).compareTo(g.getCard(0)) == 0 || 
					player1deck.getCard(1).compareTo(g.getCard(0)) == 0 ||
					player2deck.getCard(0).compareTo(g.getCard(0)) == 0 || 
					player2deck.getCard(1).compareTo(g.getCard(0)) == 0  )
			{
				g = new Deck(1);
			}
			else{
				river.add(g.getCard(0).cardToCardButton(g.getCard(0)));
				river1.add(g.getCard(0).cardToCardButton(g.getCard(0)));
			}
		}
		while(flop.size() == 3 && turn.size() != 1){
			if(player1deck.getCard(0).compareTo(f.getCard(0)) == 0 || 
					player1deck.getCard(1).compareTo(f.getCard(0)) == 0 ||
					player2deck.getCard(0).compareTo(f.getCard(0)) == 0 || 
					player2deck.getCard(1).compareTo(f.getCard(0)) == 0  )
			{
				f = new Deck(1);
			}
			else{
				turn.add(f.getCard(0).cardToCardButton(f.getCard(0)));
				turn1.add(f.getCard(0).cardToCardButton(f.getCard(0)));
			}
		}
		while(flop.size() < 3){
			if(player1deck.getCard(0).compareTo(d.getCard(0)) == 0 || 
					player1deck.getCard(1).compareTo(d.getCard(0)) == 0 ||
					player2deck.getCard(0).compareTo(d.getCard(0)) == 0 || 
					player2deck.getCard(1).compareTo(d.getCard(0)) == 0  )
			{
				d = new Deck(3);
			}
			else{
				if((player1deck.getCard(0).compareTo(d.getCard(1)) == 0 || 
						player2deck.getCard(1).compareTo(d.getCard(1)) == 0 ||
						player2deck.getCard(0).compareTo(d.getCard(1)) == 0 || 
						player1deck.getCard(1).compareTo(d.getCard(1)) == 0  ))
						{
					d = new Deck(3);
				}
				else{
					if((player1deck.getCard(0).compareTo(d.getCard(2)) == 0 || 
							player2deck.getCard(1).compareTo(d.getCard(2)) == 0 ||
							player2deck.getCard(0).compareTo(d.getCard(2)) == 0 || 
							player1deck.getCard(1).compareTo(d.getCard(2)) == 0  )){
						d = new Deck(3);
					}
					else{
						flop.add(d.getCard(0).cardToCardButton(d.getCard(0)));
						flop.add(d.getCard(1).cardToCardButton(d.getCard(1)));
						flop.add(d.getCard(2).cardToCardButton(d.getCard(2)));
						flop1.add(d.getCard(0).cardToCardButton(d.getCard(0)));
						flop1.add(d.getCard(1).cardToCardButton(d.getCard(1)));
						flop1.add(d.getCard(2).cardToCardButton(d.getCard(2)));
					}
				}
			}
			
		}
		if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
			for(int i = 0; i<flop.size(); i++){
				player1pane.add(flop.get(i), 0, 8+i);
			}
			player1pane.add(turn.get(0), 0, 11);
			player1pane.add(river.get(0), 0, 12);
		}
		else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
			for(int i = 0; i<flop.size(); i++){
				player1pane.add(flop.get(i), 0, 8+i);
			}
			player1pane.add(turn.get(0), 0, 11);
			
		}
		else if(flop.size() == 3 && turn.size() != 1){
			for(int i = 0; i<flop.size(); i++){
				player1pane.add(flop.get(i), 0, 8+i);
			}
			
		}
	}
	++turns;
	nextTurn.setOnAction(p ->{
		primary.setScene(player1);
});
});
		
	
	}
	
	
	public void cardPicker(int x){
		if(x == 1){
			Label l2 = new Label("Pick 5 cards to add to your hand");
			player1pane.add(l2, 0,  20);
						primary.setScene(player1);
						
						//Label l2 = new Label("Pick 5 Cards for Your Hand");
						//player1pane.add(l2, 0, 20);
						flop.get(0).setOnAction(m->{
							flop.get(0).disarm();
							flop.get(0).setBlendMode(BlendMode.BLUE);
							player1Cards.add(flop.get(0));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								cardPicker(2);
								//primary.setScene(player2);
							}
							
							else{
								primary.setScene(player1);
							}
							//flop.get(0).setBackground(Color.BISQUE);
						});
						flop.get(1).setOnAction(m->{
							flop.get(1).disarm();
							flop.get(1).setBlendMode(BlendMode.BLUE);
							player1Cards.add(flop.get(1));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								cardPicker(2);
							}
							
							else{
								primary.setScene(player1);
							}
							
							//flop.get(0).setBackground(Color.BISQUE);
						});
						flop.get(2).setOnAction(m->{
							flop.get(2).disarm();
							flop.get(2).setBlendMode(BlendMode.BLUE);
							player1Cards.add(flop.get(2));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								cardPicker(2);
								//primary.setScene(player2);
							}
							
							else{
								primary.setScene(player1);
							}
							
							//flop.get(0).setBackground(Color.BISQUE);
						});
						turn.get(0).setOnAction(m->{
							turn.get(0).disarm();
							turn.get(0).setBlendMode(BlendMode.BLUE);
							player1Cards.add(turn.get(0));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								cardPicker(2);
								//primary.setScene(player2);
							}
							
							else{
								primary.setScene(player1);
							}
							//flop.get(0).setBackground(Color.BISQUE);
						});
						river.get(0).setOnAction(m->{
							river.get(0).disarm();
							river.get(0).setBlendMode(BlendMode.BLUE);
							player1Cards.add(river.get(0));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								cardPicker(2);
								//primary.setScene(player2);
							}
							
							else{
								primary.setScene(player1);
							}
							//flop.get(0).setBackground(Color.BISQUE);
						});
						player1TwoCards.get(0).setOnAction(m ->{
							player1TwoCards.get(0).disarm();
							player1TwoCards.get(0).setBlendMode(BlendMode.BLUE);
							player1Cards.add(player1TwoCards.get(0));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								//primary.setScene(player2);
							cardPicker(2);
							}
							
							else{
								primary.setScene(player1);
							}
						});
						player1TwoCards.get(1).setOnAction(m ->{
							player1TwoCards.get(1).disarm();
							player1TwoCards.get(1).setBlendMode(BlendMode.BLUE);
							player1Cards.add(player1TwoCards.get(1));
							if(player1Cards.size() == 5){
								if(flop.size() == 3 && turn.size() == 1 && river.size() == 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									player2pane.add(river1.get(0), 0, 12);
								}
								else if(flop.size() == 3 && turn.size() == 1 && river.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
									player2pane.add(turn1.get(0), 0, 11);
									
								}
								else if(flop.size() == 3 && turn.size() != 1){
									for(int i = 0; i<flop.size(); i++){
										player2pane.add(flop1.get(i), 0, 8+i);
									}
								}
								++cardpick;
								//primary.setScene(player2);
								cardPicker(2);
							}
							
							else{
								primary.setScene(player1);
							}
							
						});
					}
		
		if(x == 2){
			Label l2 = new Label("Pick 5 Cards for Your Hand");
			player2pane.add(l2, 0, 20);
				Label l3 = new Label("Pick 5 cards to add to your hand");
				player1pane.add(l3, 0,  20);
							primary.setScene(player2);
							
							flop1.get(0).setOnAction(n->{
								flop1.get(0).disarm();
								flop1.get(0).setBlendMode(BlendMode.BLUE);
								player2Cards.add(flop1.get(0));
								if(player2Cards.size() == 5){
										
									
										player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList();
										 tempdeck2 = new ArrayList();
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1((temp1));
										Pair<String, Integer> p2pair = checkHand2((temp2));
										System.out.println(p1pair + " ");
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
									if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								//primary.setScene(player1end);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q ->{
									System.exit(1);
								});
							}
									else{
										player1endpane.add(new Label("Player 2 Wins: " + p2pair.getKey()), 0, 0);
										Button playagain = new Button("Play Again");
										Button closegame = new Button("Close Game");
										player1endpane.add(playagain,0,  2);
										player1endpane.add(closegame,  0,  4);
										playagain.setOnAction(q -> {
											try {
												start(primary);
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											
										});
										closegame.setOnAction(q ->{
											System.exit(1);
										});
									}
									primary.setScene(player1end);	
										}
								
								else{
									primary.setScene(player2);
								}
								//flop.get(0).setBackground(Color.BISQUE);
							});
							flop1.get(1).setOnAction(n->{
								flop1.get(1).disarm();
								flop1.get(1).setBlendMode(BlendMode.BLUE);
								player2Cards.add(flop1.get(1));
								if(player2Cards.size() == 5){
									
									++cardpick;	
									
										player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList();
										tempdeck2 = new ArrayList();
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1((temp1));
										Pair<String, Integer> p2pair = checkHand2((temp2));
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								//primary.setScene(player1end);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q ->{
									System.exit(1);
								});
							}
										else{
											player1endpane.add(new Label("Player 2 Wins: " + p2pair.getKey()), 0, 0);
											Button playagain = new Button("Play Again");
											Button closegame = new Button("Close Game");
											player1endpane.add(playagain,0,  2);
											player1endpane.add(closegame,  0,  4);
											playagain.setOnAction(q -> {
												try {
													start(primary);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
											});
											closegame.setOnAction(q ->{
												System.exit(1);
											});
										}
										primary.setScene(player1end);	
								}
								
								else{
									primary.setScene(player2);
								}
								
								//flop.get(0).setBackground(Color.BISQUE);
							});
							flop1.get(2).setOnAction(n->{
								flop1.get(2).disarm();
								flop1.get(2).setBlendMode(BlendMode.BLUE);
								player2Cards.add(flop1.get(2));
								if(player2Cards.size() == 5){
									
									++cardpick;
									
										player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList();
										tempdeck2 = new ArrayList();
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1(temp1);
										Pair<String, Integer> p2pair = checkHand2(temp2);
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								//primary.setScene(player1end);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame, 0, 4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q ->{
									System.exit(1);
								});
							}
										else{
											player1endpane.add(new Label("Player 2 Wins: " + p2pair.getKey()), 0, 0);
											Button playagain = new Button("Play Again");
											Button closegame = new Button("Close Game");
											player1endpane.add(playagain,0,  2);
											player1endpane.add(closegame,  0,  4);
											playagain.setOnAction(q -> {
												try {
													start(primary);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
											});
											closegame.setOnAction(q ->{
												System.exit(1);
											});
										}
										primary.setScene(player1end);	
								}
								
								else{
									primary.setScene(player2);
								}
								
								//flop.get(0).setBackground(Color.BISQUE);
							});
							turn1.get(0).setOnAction(n->{
								turn1.get(0).disarm();
								turn1.get(0).setBlendMode(BlendMode.BLUE);
								player2Cards.add(turn1.get(0));
								if(player2Cards.size() == 5){
								
									++cardpick;
									
										player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList();
										tempdeck2 = new ArrayList();
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1(temp1);
										Pair<String, Integer> p2pair = checkHand2(temp2);
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q -> {
									System.exit(1);
								});
							}
										else{
											player1endpane.add(new Label("Player 2 Wins: " + p2pair.getKey()), 0, 0);
											Button playagain = new Button("Play Again");
											Button closegame = new Button("Close Game");
											player1endpane.add(playagain,0,  2);
											player1endpane.add(closegame,  0,  4);
											playagain.setOnAction(q -> {
												try {
													start(primary);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
											});
											closegame.setOnAction(q ->{
												System.exit(1);
											});
										}
										primary.setScene(player1end);			
								}
								
								else{
									primary.setScene(player2);
								}
								//flop.get(0).setBackground(Color.BISQUE);
							});
							river1.get(0).setOnAction(n->{
								river1.get(0).disarm();
								river1.get(0).setBlendMode(BlendMode.BLUE);
								player2Cards.add(river1.get(0));
								if(player2Cards.size() == 5){
									
									++cardpick;
									
									player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList(5);
										tempdeck2 = new ArrayList(5);
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1(temp1);
										Pair<String, Integer> p2pair = checkHand2(temp2);
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q -> {
									System.exit(1);
								});
							}
										else{
											player1endpane.add(new Label("Player 2 wins: " + p2pair.getKey()), 0, 0);
											Button playagain = new Button("Play Again");
											Button closegame = new Button("Close Game");
											player1endpane.add(playagain,0,  2);
											player1endpane.add(closegame,  0,  4);
											playagain.setOnAction(q -> {
												try {
													start(primary);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
											});
											closegame.setOnAction(q ->{
												System.exit(1);
											});
										}
										primary.setScene(player1end);			
								}
								else{
									primary.setScene(player2);
								}
								//flop.get(0).setBackground(Color.BISQUE);
							});
							player2TwoCards.get(0).setOnAction(n ->{
								player2TwoCards.get(0).disarm();
								player2TwoCards.get(0).setBlendMode(BlendMode.BLUE);
								player2Cards.add(player2TwoCards.get(0));
								if(player2Cards.size() == 5){
									
									++cardpick;
									
										player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList(5);
										tempdeck2 = new ArrayList(5);
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1(temp1);
										Pair<String, Integer> p2pair = checkHand2(temp2);
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								});
								closegame.setOnAction(q ->{
									System.exit(1);
								});
							}
										else{
											player1endpane.add(new Label("Player 2 wins: " + p2pair.getKey()), 0, 0);
											Button playagain = new Button("Play Again");
											Button closegame = new Button("Close Game");
											player1endpane.add(playagain,0,  2);
											player1endpane.add(closegame,  0,  4);
											playagain.setOnAction(q -> {
												try {
													start(primary);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
											});
											closegame.setOnAction(q ->{
												System.exit(1);
											});
										}
										primary.setScene(player1end);			
									}
								
								
								else{
									primary.setScene(player2);
								}
							});
							player2TwoCards.get(1).setOnAction(n ->{
								player2TwoCards.get(1).disarm();
								player2TwoCards.get(1).setBlendMode(BlendMode.BLUE);
								player2Cards.add(player2TwoCards.get(1));
								if(player2Cards.size() == 5){
									
									++cardpick;
									player1endpane = new GridPane();
										player1end = new Scene(player1endpane, 400, 400);
										tempdeck = new ArrayList();
										tempdeck2 = new ArrayList();
										for(int i = 0; i<player1Cards.size(); i++){
											tempdeck.add(player1Cards.get(i).cardButtonToCard(player1Cards.get(i)));
											tempdeck2.add(player2Cards.get(i).cardButtonToCard(player2Cards.get(i)));
										}
										//handolomew h1 = new handolomew(tempdeck);
										//handolomew h2 = new handolomew(tempdeck2);
										temp1 = listConverter(tempdeck);
										temp2 = listConverter(tempdeck2);
										Pair<String, Integer> p1pair = checkHand1(temp1);
										Pair<String, Integer> p2pair = checkHand2(temp2);
										System.out.println(p1pair + " " + p2pair);
										Label l0 = new Label ("You won: " + currentbettingvalue);
										player1endpane.add(l0, 0, 10);
										if(p1pair.getValue() > p2pair.getValue()){
								player1endpane.add(new Label("PLayer 1 Wins: " + p1pair.getKey()), 0, 0);
							Button playagain = new Button("Play Again");
							Button closegame = new Button("Close Game");
							player1endpane.add(playagain,0,  2);
							player1endpane.add(closegame,  0,  4);
							playagain.setOnAction(q -> {
								try {
									start(primary);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							});
							closegame.setOnAction(q ->{
								System.exit(1);
							});
							}
							else{
								player1endpane.add(new Label("PLayer 2 Wins: " + p2pair.getKey()), 0, 0);
								Button playagain = new Button("Play Again");
								Button closegame = new Button("Close Game");
								player1endpane.add(playagain,0,  2);
								player1endpane.add(closegame,  0,  4);
								playagain.setOnAction(q -> {
									try {
										start(primary);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								});
								closegame.setOnAction(q ->{
									System.exit(1);
								});
							}
								primary.setScene(player1end);			
									//primary.setScene(player1);
								}
								
								else{
									primary.setScene(player2);
								}
							
							});
		}
	}
	
	public Pair<String, Integer> checkHand1(Card [] hand){
		if(allOneSuit(temp1) && allInOrder(temp1))
			{return new Pair("Straight Flush", 8);}
		if(temp1[0].getRank() == temp1[3].getRank() || temp1[1].getRank() == temp1[4].getRank())
			{return new Pair("Four of a Kind", 7);}
		if((temp1[0].getRank() == hand[2].getRank() && temp1[3].getRank() == temp1[4].getRank()) || (temp1[0].getRank() == temp1[1].getRank() && temp1[2].getRank() == temp1[4].getRank()))
			{return new Pair("Full House", 6);}
		if(allOneSuit(temp1))
			{return new Pair("Flush", 5);}
		if(allInOrder(temp1))
			{return new Pair("Straight",4);}
		if(temp1[0].getRank() == temp1[2].getRank() || temp1[1].getRank() == temp1[3].getRank() || temp1[2].getRank() == temp1[4].getRank())
			{return new Pair("Three of a kind", 3);}
		if((temp1[0].getRank() == temp1[1].getRank() && (temp1[2].getRank() == temp1[3].getRank() || temp1[3].getRank() == temp1[4].getRank())) || (temp1[1].getRank() == temp1[2].getRank() && temp1[3].getRank() == temp1[4].getRank()))
			{return new Pair("Two Pair", 2);}
		if(temp1[0].getRank() == temp1[1].getRank() || temp1[1].getRank() == temp1[2].getRank() || temp1[2].getRank() == temp1[3].getRank() || temp1[3].getRank() == temp1[4].getRank())
			{return new Pair("One Pair", 1);}
		return new Pair("High Card " + hand[4].getRank() + " OF " + hand[4].getSuit(), 0);
	}
public Pair<String, Integer> checkHand2(Card [] hand){
	if(allOneSuit(temp2) && allInOrder(temp2))
	{return new Pair("Straight Flush", 8);}
if(temp2[0].getRank() == temp2[3].getRank() || temp2[1].getRank() == temp2[4].getRank())
	{return new Pair("Four of a Kind", 7);}
if((temp2[0].getRank() == hand[2].getRank() && temp2[3].getRank() == temp2[4].getRank()) || (temp2[0].getRank() == temp2[1].getRank() && temp2[2].getRank() == temp2[4].getRank()))
	{return new Pair("Full House", 6);}
if(allOneSuit(temp2))
	{return new Pair("Flush", 5);}
if(allInOrder(hand))
	{return new Pair("Straight",4);}
if(temp2[0].getRank() == temp2[2].getRank() || temp2[1].getRank() == temp2[3].getRank() || temp2[2].getRank() == temp2[4].getRank())
	{return new Pair("Three of a kind", 3);}
if((temp2[0].getRank() == temp2[1].getRank() && (temp2[2].getRank() == temp2[3].getRank() || temp2[3].getRank() == temp2[4].getRank())) || (temp2[1].getRank() == temp2[2].getRank() && temp2[3].getRank() == temp2[4].getRank()))
	{return new Pair("Two Pair", 2);}
if(temp2[0].getRank() == temp2[1].getRank() || temp2[1].getRank() == temp2[2].getRank() || temp2[2].getRank() == temp2[3].getRank() || temp2[3].getRank() == temp2[4].getRank())
	{return new Pair("One Pair", 1);}
return new Pair("High Card " + hand[4].getRank() + " OF " + hand[4].getSuit(), 0);
}
	public boolean allInOrder(Card [] hand){
		if(hand[0].getRank() + 1 == hand[1].getRank() && hand[1].getRank() + 1 == hand[2].getRank() && hand[2].getRank() + 1 == hand[3].getRank() && hand[3].getRank() + 1 == hand[4].getRank())
				return true;
		if(hand[0].getRank() == 0 && hand[1].getRank() == 9 && hand[2].getRank() == 10 && hand[3].getRank() == 11 && hand[4].getRank() == 12)
				return true;
		if(hand[0].getRank() == 0 && hand[1].getRank() == 1 && hand[2].getRank() == 10 && hand[3].getRank() == 11 && hand[4].getRank() == 12)
			return true;
		if(hand[0].getRank() == 0 && hand[1].getRank() == 1 && hand[2].getRank() == 2 && hand[3].getRank() == 11 && hand[4].getRank() == 12)
			return true;
		if(hand[0].getRank() == 0 && hand[1].getRank() == 1 && hand[2].getRank() == 2 && hand[3].getRank() == 2 && hand[4].getRank() == 12)
			return true;
		return false;
	}
	
	public boolean allOneSuit(Card [] hand){
		for(int i = 0; i<temp1.length; i++){
			System.out.println(temp1[i]);
		}
		if(temp1[0].getSuit().equals(temp1[1].getSuit()) && temp1[0].getSuit().equals(temp1[2].getSuit()) &&
temp1[0].getSuit().equals(temp1[3].getSuit()) && temp1[0].getSuit().equals(temp1[4].getSuit()))
			return true;
		if(temp1[0].getSuit().equals(temp1[1].getSuit()) && temp1[0].getSuit().equals(temp1[2].getSuit()) &&
				temp1[0].getSuit().equals(temp1[3].getSuit()) && temp1[0].getSuit().equals(temp1[4].getSuit()))
							return true;
		return false;
	}
	
	public Card [] listConverter(ArrayList<Card> x){
		Card [] temp = new Card[10];
		Collections.sort(x);
		System.out.println(x);
		for(int i = 0; i< x.size(); i++){
			temp[i] = x.get(i);
		}
		return temp;
	}
		
	}