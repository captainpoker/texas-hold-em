
import java.util.Arrays;
import java.util.List;

import javafx.scene.control.Button;

public class Card implements Comparable<Card> {
	private int suit;//stores the suit of the card
	private int rank; //stores the rank of the card
	//all suits of the cards in order of rank
	public final static List<String> SUITS = Arrays.asList("Clubs", "Diamonds", "Hearts", "Spades"); 
	
	public final static List<String> RANKS = Arrays.asList("2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"); //all ranks in order from least to greatest

	//card class has two constructors, one takes integers, one takes strings. 
	//The second one uses the conversion methods to convert from int to string
	public Card(int suit, int rank){ 
		this.setSuit(suit);
		this.setRank(rank);
	}
	
	public Card(String suit, String rank){
		this(suitToInt(suit), rankToInt(rank));
	}
	//converts given String to its rank
	public static int rankToInt(String rank1){ 
		for(int i = 0; i<RANKS.size(); i++){
			if(rank1.equals(RANKS.get(i))){
				return i+1;
			}
		}
		return -1;
	}
	//converts given String to its rank of suit
	public static int suitToInt(String suit1) { 
		for(int i = 0; i<SUITS.size(); i++){
			if(suit1.equals(SUITS.get(i))){
				return i+1;
			}
		}
		return -1;
	}
	//returns suit based on integer given
	public static String intToSuit(int suit1){ 
		return SUITS.get(suit1-1);
	}
	//returns rank based on integer given
	public static String intToRank(int rank1){
		return RANKS.get(rank1-1);
	}

	public String getSuit() {
		return intToSuit(suit);
	}
public int getSuitNum(){
	return suit;
}
	public void setSuit(int suit) {
		this.suit = suit;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	public int compareTo(Card other){

		if (rank == other.rank){

		return suit-other.suit;

		}

		return rank - other.rank;

		}
	public CardButton cardToCardButton(Card x){
		return new CardButton(x.intToRank(x.getRank()) + " of " + (x.getSuit()));
	}
	
	
	public String toString(){

		return "\n" + RANKS.get(rank-1) + " of " + SUITS.get(suit-1);

		}
}
