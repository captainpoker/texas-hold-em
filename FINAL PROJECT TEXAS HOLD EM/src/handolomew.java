import java.util.ArrayList;
import java.util.Collections;

import javafx.util.Pair;

public class handolomew {
	
	private int two = 0;                                         //for method allInOrder
	private int three = 1;                                       //^
	private int four = 2;                                        //^
	private int five = 3;                                        //^
	private int jack = 9;                                        //^
	private int queen = 10;                                      //^
	private int king = 11;                                       //^
	private int ace = 12;                                        //^
	private Card[] hand;                                         //hand ordered by rank
	private Pair<String, Integer> best;                          //best holds the hand's value
	
	public handolomew(ArrayList<Card> x){
		Collections.sort(x);
		this.hand = new Card[5];
		for(int i = 0; i < 5; ++i)hand[i] = x.get(i);
		this.best = checkHand();
	}
	
	public Pair<String, Integer> checkHand(){
		if(allOneSuit() && allInOrder())
			{return new Pair("Straight Flush", 8);}
		if(hand[0].getRank() == hand[3].getRank() || hand[1].getRank() == hand[4].getRank())
			{return new Pair("Four of a Kind", 7);}
		if((hand[0].getRank() == hand[2].getRank() && hand[3].getRank() == hand[4].getRank()) || (hand[0].getRank() == hand[1].getRank() && hand[2].getRank() == hand[4].getRank()))
			{return new Pair("Full House", 6);}
		if(allOneSuit())
			{return new Pair("Flush", 5);}
		if(allInOrder())
			{return new Pair("Straight",4);}
		if(hand[0].getRank() == hand[2].getRank() || hand[1].getRank() == hand[3].getRank() || hand[2].getRank() == hand[4].getRank())
			{return new Pair("Three of a kind", 3);}
		if((hand[0].getRank() == hand[1].getRank() && (hand[2].getRank() == hand[3].getRank() || hand[3].getRank() == hand[4].getRank())) || (hand[1].getRank() == hand[2].getRank() && hand[3].getRank() == hand[4].getRank()))
			{return new Pair("Two Pair", 2);}
		if(hand[0].getRank() == hand[1].getRank() || hand[1].getRank() == hand[2].getRank() || hand[2].getRank() == hand[3].getRank() || hand[3].getRank() == hand[4].getRank())
			{return new Pair("One Pair", 1);}
		return new Pair("High Card " + hand[4].getRank() + " OF " + hand[4].getSuit(), 0);
	}

	public boolean allInOrder(){
		if(hand[0].getRank() + 1 == hand[1].getRank() && hand[1].getRank() + 1 == hand[2].getRank() && hand[2].getRank() + 1 == hand[3].getRank() && hand[3].getRank() + 1 == hand[4].getRank())
				return true;
		if(hand[0].getRank() == two && hand[1].getRank() == jack && hand[2].getRank() == queen && hand[3].getRank() == king && hand[4].getRank() == ace)
				return true;
		if(hand[0].getRank() == two && hand[1].getRank() == three && hand[2].getRank() == queen && hand[3].getRank() == king && hand[4].getRank() == ace)
			return true;
		if(hand[0].getRank() == two && hand[1].getRank() == three && hand[2].getRank() == four && hand[3].getRank() == king && hand[4].getRank() == ace)
			return true;
		if(hand[0].getRank() == two && hand[1].getRank() == three && hand[2].getRank() == four && hand[3].getRank() == five && hand[4].getRank() == ace)
			return true;
		return true;
	}
	
	public boolean allOneSuit(){
		if(hand[0].getSuit().equals(hand[1].getSuit()) && hand[0].getSuit().equals(hand[2].getSuit()) && hand[0].getSuit().equals(hand[3].getSuit()) && hand[0].getSuit().equals(hand[4].getSuit()))
			return true;
		return false;
	}
	
	public String toString(){
		return best.getKey();
	}
}