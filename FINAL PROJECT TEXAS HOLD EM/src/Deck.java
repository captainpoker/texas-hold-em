import java.awt.Button;
import java.util.*;

public class Deck{

private ArrayList<Card> hand;//what cards are in your hand currently

public static final int FULLDECK = 52;//a full deck of cards

//creates a full deck, shuffles the cards, and removes all but what is called for in the constructor call

public Deck (int n){

Random r1 = new Random();

hand = new ArrayList();

for(int i = 0; i<Card.RANKS.size(); i++){

for(int k = 0; k<Card.SUITS.size(); k++){

hand.add(new Card(Card.SUITS.get(k), Card.RANKS.get(i)));

}

}

Collections.shuffle(hand);

if(hand.size()>n){

hand.remove(r1.nextInt(hand.size()));

}

}

//calls the upper constructor with zero cards

public Deck() {

this(0);

}
public int getLength(){
	return hand.size();
}
//takes the rank and the suit, and finds that card in the hand. If it is not there, it returns 0

public int findCard(String rank, String suit){

Card c = new Card(rank, suit);

for(int i = 0; i< hand.size(); ++i){

if(hand.get(i).equals(c)){

return i;

}

}

return -1;

}

//sorts deck starting with clubs, then diamonds, then spades, then hearts. Once in suits,

//cards go in number order

public void sortDeck(){

Collections.sort(hand);

}

// arranges the cards in the deck randomly.

public void shuffleDeck(){

Collections.shuffle(hand);

}

//calls card class's toString to list out cards in their order of the deck

public String toString(){

String ending = " ";

if(hand.size()==0){

return "";

}

for(Card c : hand){

ending += c.toString();

}

return ending + "\n \n New Hand \n";

}

//Adds given card and returns true if the card was added

//if there is a replica, card is not added and false is returned

public boolean addCard(String cardRank, String cardSuit){

Card other = new Card(cardRank, cardSuit);

for(int i = 0; i<hand.size(); i++){

if(hand.get(i).equals(other)){

return false;

}

}

hand.add(0, new Card(cardRank, cardSuit));

return true;

}

//if found, the card is removed and true is returned

//if not found, false is returned

public boolean removeCard(String cardRank, String cardSuit){

Card removal = new Card(cardRank, cardSuit);

for(int i = 0; i<hand.size(); i++){

if(hand.get(i).equals(removal)){

hand.remove(i);

return true;

}

}

return false;

}

//returns true if the deck has 52 cards in it

public boolean isFull(){

if(hand.size() == FULLDECK){

return true;

}

return false;

}

//returns true if the deck is currently sorted

public boolean isSorted(){

for(int i = 0; i<hand.size()-1; ++i){

if(hand.get(i).compareTo(hand.get(i+1))==1){

return false;

}

}

return true;

}

public Card getCard(int i){
	return hand.get(i); 
}

//@Override



// end class Deck

}
