import java.util.ArrayList;

public class WinningStates {
	public boolean straightFlush(ArrayList<Card> x){
		boolean samesuits = true;
		for(int i = 0; i<x.size()-1; i++){
			if(!x.get(i).getSuit().equals(x.get(i).getSuit())){
				samesuits = false;
			}
		}
		boolean descending = true;
		if(samesuits){
			for(int k = 0; k<x.size(); k++){
				if(!(x.get(k).getRank()-1 == x.get(k).getRank())){
					descending = false;
				}
			}
		}
		if(samesuits && descending){
			return true;
		}
		else{
			return false;
		}
	}
}
